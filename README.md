# MNE examples for EEG ERP analysis #

 - [Read and display eeg data from .csv](./mne-raw-read.py)
 - [Find events from marker (stimuli) channel](./mne-raw-read-events.py)