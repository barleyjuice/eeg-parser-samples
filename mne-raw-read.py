import mne
import pandas as pd
import numpy as np

# Reading

# channel names extracted from log file header
eeg_channels = "EEG 1,EEG 2,EEG 3,EEG 4,EEG 5,EEG 6,EEG 7,EEG 8".split(",")

# read csv file, extract only necessary columns and convert it to numpy structure
data = np.transpose(pd.read_csv("sample.csv")[[*eeg_channels]].to_numpy())

# Converting to MNE format

# info struct is necessary to create before constructing raw data. 256 is our frequency
info = mne.create_info(ch_names=eeg_channels, sfreq=256, ch_types="eeg")

# perform conversion
mne_raw = mne.io.RawArray(data, info)

# Analysis

# apply filtration
mne_raw.filter(l_freq=8, h_freq=40, n_jobs=4)

# render interactive plot to visually analyse data
scaling = {'eeg' : 45} # 45 is well comprehensible
mne_raw.plot(n_channels=len(eeg_channels), scalings=scaling, show=True, title='em1', block=True)