# https://mne.tools/stable/auto_tutorials/raw/20_event_arrays.html#tut-event-arrays

import mne
import pandas as pd
import numpy as np

# Reading

# channel names extracted from log file header
eeg_channels = "EEG 1,EEG 2,EEG 3,EEG 4,EEG 5,EEG 6,EEG 7,EEG 8".split(",")
stim_ch_name = "STI01"

# stimuli info must be located in special column in log-file
# in current log sample there is no stimuli info, so, for the demonstration purposes 
# we artificially create the stim column and place here some stimuli markers.
# in real case such column must be predefined during the experimental session

# read csv file, extract only necessary columns and convert it to numpy structure
# demo case only: read eeg channels only, as we have no stimuli one
data = pd.read_csv("sample.csv")[[*eeg_channels]].to_numpy()
# real case with embedded stimuli column: need to include stimuli channel name in channel_names list
# data = pd.read_csv("sample.csv")[[*eeg_channels, stim_ch_name]].to_numpy()

n_samples = data.shape[0]

# demo case only: stimuli events simulation
stim_event1_indices = [128, 256, 512, 1024] # where event markers will be placed at
stim_ch_data = np.zeros(n_samples) # initial contents of stimuli column
stim_event1_marker = 5 # 1 is too little - no events are found, 10 is too much - there are extra events, but we can ignore them
for i in stim_event1_indices:
    stim_ch_data[i] = stim_event1_marker 
# demo case only: append stimuli column to main dataset
data = np.c_[data, stim_ch_data]

# Converting to MNE format

# info struct is necessary to create before constructing raw data. 256 is our frequency
info = mne.create_info(ch_names=[*eeg_channels, stim_ch_name], sfreq=256, ch_types="eeg")

# perform conversion
data = np.transpose(data)
mne_raw = mne.io.RawArray(data, info)

# Extracting events

# find events from stimuli channel
events = mne.find_events(mne_raw, stim_channel=stim_ch_name)

print(events)

event_dict = {"sample events": stim_event1_marker}

# plot the events
fig = mne.viz.plot_events(events, sfreq=mne_raw.info['sfreq'],
                         first_samp=mne_raw.first_samp, event_id=event_dict)
fig.subplots_adjust(right=0.7)  # make room for legend

# plotting events and data together
scaling = {'eeg' : 45} # 45 is well comprehensible
mne_raw.plot(n_channels=len(eeg_channels), scalings=scaling, show=True, title='em1', block=True, \
    events=events, color='gray', event_color={stim_event1_marker: 'r'})